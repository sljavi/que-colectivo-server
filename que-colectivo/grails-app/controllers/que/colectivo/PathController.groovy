package que.colectivo

import grails.converters.JSON

class PathController {
    static scaffold = true

	def jsonpList() {
		def paths = Path.list()
		def pathsAsJson = paths as JSON
        render 'callback({"result": ' + pathsAsJson.toString() + '})'
	}
}
