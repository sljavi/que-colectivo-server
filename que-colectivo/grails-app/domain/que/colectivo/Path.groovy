package que.colectivo

class Path {
	String code

	String name

	String color

	String points

    static constraints = {
		points size: 1..5000
    }
}
