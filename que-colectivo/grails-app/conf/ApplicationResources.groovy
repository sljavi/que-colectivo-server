modules = {
    application {
        resource url:'js/application.js'
    }
    map {
        resource url: '/js/script.js'
        resource url: '/css/style.css'
    }
}