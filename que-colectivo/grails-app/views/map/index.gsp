<!DOCTYPE html>
  <head>
    <title>Qué colectivo me tomo?</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
    <link href="css/style.css" rel="stylesheet"/>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script src="js/script.js"></script>
    
    <r:require modules="map"/>
  </head>
  <body>
    <div class="begin-end-container" style="display: none">
      <h1>Qué colectivo me tomo?</h1>
      <p>
        Escribe la dirección de origen y destino, o señala los puntos en el mapa.
      </p>
      <label>Desde</label>
      <input id="begin" type="text" class="form-control" placeholder="Origen">
      <label>Hasta</label>
      <input id="end" type="text" class="form-control" placeholder="Destino">
      
      <h4 style="display: none">Recorridos Encontrados</h4>
      <div style="display: none" class="paths btn-group-vertical">

      </div>
    </div>
    <div id="map-canvas"></div>
  </body>
</html>