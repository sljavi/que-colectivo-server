/*global _, google, $ */
var
  queColectivoServerEndpoint = "http://localhost:8080/que-colectivo",
  sanLuis = {
    lng: -66.312962,
    lat: -33.300000,
    minLat: -33.34573003669736,
    maxLat: -33.253618220151075,
    minLng: -66.39930725097656,
    maxLng: -66.21597290039062,
    minZoom: 12,
    defaultZoom: 14
  },
  paths = [],
  subPaths = [],
  sanLuisLatLng,
  map,
  markers = {
    begin: null,
    end: null,
    start: null,
    finish: null
  },
  currentRenderedPath,
  initializeMarkersListener, geocoder, flightPath;

function renderPath(path, opacity, color, weight, zIndex) {
  var latLngPath;
  
  latLngPath = _.map(path, function (valuePair) {
    return new google.maps.LatLng(valuePair[0], valuePair[1]);
  });

  return new google.maps.Polyline({
    path: latLngPath,
    editable: false,
    geodesic: true,
    strokeColor: color,
    strokeOpacity: opacity,
    strokeWeight: weight,
    map: map,
    zIndex: zIndex
  });
}

function getAddressByLatLng(latlng, callback) {
  geocoder.geocode({'latLng': latlng}, function (results, status) {
    var address = null;
    if (status === google.maps.GeocoderStatus.OK && !_.isEmpty(results)) {
      address = _.first(_.first(results).formatted_address.split(','));
    }
    callback(address);
  });
}

function getLocationsByAddress(address, callback) {
  geocoder.geocode({ 'address': address}, function (locations, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      callback(locations);
    } else {
      callback([]);
    }
  });
}

function getDistance(begin, end) {
  return  Math.sqrt(Math.pow(end[0] - begin[0], 2) +  Math.pow(end[1] - begin[1], 2));
}

function getSubPath(path, begin, end) {
  var
    start = {
      distance: Number.MAX_VALUE,
      index: null,
    },
    finish = {
      distance: Number.MAX_VALUE,
      index: null
    };

  _.each(path, function (valuePair, index) {
    var endDistance = getDistance(end, valuePair);
    if (endDistance < finish.distance) {
      finish.distance = endDistance;
      finish.index = index;
    }
  });

  _.each(path.slice(0, finish.index + 1), function (valuePair, index) {
    var beginDistance = getDistance(begin, valuePair);
    if (beginDistance < start.distance) {
      start.distance = beginDistance;
      start.index = index;
    }
  });

  return {
    distanceToWalk: start.distance + finish.distance,
    subPath: path.slice(start.index, finish.index + 1)
  };
}

function updateSubPaths() {
  var begin, end, bestPath;
  begin = [markers.begin.getPosition().lat(), markers.begin.getPosition().lng()];
  end = [markers.end.getPosition().lat(), markers.end.getPosition().lng()];
  _.each(paths, function (path) {
    path.subPath = getSubPath(path.points, begin, end);
  });
}

function getBestSubPath(subPaths) {
  var bestSubPath = _.first(subPaths);
  _.each(subPaths, function (subPath) {
    if (bestSubPath.distanceToWalk > subPath.distanceToWalk) {
      bestSubPath = subPath;
    }
  });
  return bestSubPath;
}

function sortPaths() {
  paths = _.sortBy(paths, function (path) {
    return path.subPath.distanceToWalk;
  });
}

function selectSubPath(path) {
  var
    $pathList = $('.paths'),
    $button = $pathList.find('.' + path.code),
    $firstCoordinates = _.first(path.subPath.subPath),
    $lastCoordinates = _.last(path.subPath.subPath);

  if (currentRenderedPath) {
    currentRenderedPath.setVisible(false);
  }
  currentRenderedPath = renderPath(path.subPath.subPath, 1, path.color, 2, 6);
  markers.start.setPosition(new google.maps.LatLng($firstCoordinates[0], $firstCoordinates[1]));
  markers.finish.setPosition(new google.maps.LatLng($lastCoordinates[0], $lastCoordinates[1]));
  markers.start.setVisible(true);
  markers.finish.setVisible(true);

  $pathList.find('button').removeClass('btn-success');
  $button.addClass('btn-success');
}

function updatePathList() {
  var $pathList = $('.paths');
  $pathList.find('button').remove();
  _.each(paths, function (path) {
    var
      $button = $('<button type="button" class="btn btn-default ' + path.code + '">' + path.name + '</button>'),
      mouseoverPath;

    $button.click(function () {
      selectSubPath(path);
    });
    $button.mouseover(function () {
      mouseoverPath = renderPath(path.subPath.subPath, 1, path.color, 2, 6);
    });
    $button.mouseout(function () {
      mouseoverPath.setVisible(false);
    });

    $('.paths').append($button);
  });
}

function showPathList() {
  $('.begin-end-container h4').show();
  $('.begin-end-container .paths').show();
}

function updatePaths() {
  if (markers.begin.getVisible() && markers.end.getVisible()) {
    showPathList();
    updateSubPaths();
    sortPaths();
    updatePathList();
    selectSubPath(_.first(paths));
  }
}

function initializeMarker(latLng, label, id) {
  var
    marker,
    updateAddress = function (address) {
      if (address) {
        marker.currentAddress = address;
        $('#' + id).prop('value', address);
      }
    },
    handleDragend = function (event) {
      var
        lat = event.latLng.lat(),
        lng = event.latLng.lng(),
        latLng = new google.maps.LatLng(lat, lng);
      getAddressByLatLng(latLng, updateAddress);
      updatePaths();
    },
    handleNewAddress = function (address) {
      marker.setVisible(true);
      if (address !== marker.currentAddress) {
        getLocationsByAddress(address, function (locations) {
          var location = _.find(locations, function (location) {
            var
              lat = location.geometry.location.lat(),
              lng = location.geometry.location.lng();

            return lat >= sanLuis.minLat && lat <= sanLuis.maxLat && lng >= sanLuis.minLng && lng <= sanLuis.maxLng;
          });
          if (location) {
            marker.currentAddress = address;
            marker.setPosition(location.geometry.location);
            updatePaths();
          }
        });
      }
    },
    handleNewPosition = function (latLng) {
      getAddressByLatLng(latLng, updateAddress);
      marker.setVisible(true);
      marker.setPosition(latLng);
      updatePaths();
    };

  marker = new google.maps.Marker({
    position: latLng,
    draggable: true,
    visible: false,
    map: map,
    icon: 'images/' + id + '.png',
    title: label,
    zIndex: 6
  });

  marker.currentAddress = "";
  marker.handleNewAddress = handleNewAddress;
  marker.handleNewPosition = handleNewPosition;
  markers[id] = marker;
  google.maps.event.addListener(markers[id], 'dragend', handleDragend);
}

function initializeStartAndFinishMarkers() {
  markers.start = new google.maps.Marker({
    position: sanLuisLatLng,
    visible: false,
    map: map,
    icon: 'images/start.png',
    title: 'Inicio',
    zIndex: 7
  });

  markers.finish = new google.maps.Marker({
    position: sanLuisLatLng,
    visible: false,
    map: map,
    icon: 'images/finish.png',
    title: 'Final',
    zIndex: 7
  });
}

//hola = ""
//flightPath.getPath().forEach(function (point) { hola = hola + "["+ point.lng() + ", " + point.lat() + "], "})
function initializeLine() {
  var flightPlanCoordinates = [new google.maps.LatLng(sanLuis.lat + 0.001, sanLuis.lng), new google.maps.LatLng(sanLuis.lat, sanLuis.lng)];
  flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    editable: true,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2,
    map: map
  });
}

function initializeForm() {
  var
    handleNewBeginAddress = function (event) {
      var address = $(event.target).prop('value');
      if (address !== "") {
        address = address + ", San Luis, Argentina";
        markers.begin.handleNewAddress(address);
      }
    },
    handleNewEndAddress = function (event) {
      var address = $(event.target).prop('value');
      if (address !== "") {
        address = address + ", San Luis, Argentina";
        markers.end.handleNewAddress(address);
      }
    };
  $('#begin').bind('keyup change', handleNewBeginAddress);
  $('#end').bind('keyup change', handleNewEndAddress);
  $('.begin-end-container').show();
}

function handleClickOnMap(event) {
  var
    lat = event.latLng.lat(),
    lng = event.latLng.lng(),
    latLng = new google.maps.LatLng(lat, lng);

  if (!markers.begin.getVisible()) {
    markers.begin.handleNewPosition(latLng);
  } else if (!markers.end.getVisible()) {
    markers.end.handleNewPosition(latLng);
  } else {
    google.maps.event.removeListener(initializeMarkersListener);
  }
}

function loadPaths(callback) {
  $.ajax({
    dataType: "jsonp",
    url: queColectivoServerEndpoint + "/path/jsonpList",
    crossDomain: true,
    jsonpCallback: 'callback',
    success: function (data) {
      paths = data.result;
      _.each(paths, function (path) {
        path.points = eval(path.points);
        path.subPath = null;
      });
      callback();
    }
  });
}

function initialize() {
  sanLuisLatLng = new google.maps.LatLng(sanLuis.lat, sanLuis.lng);
  
  var
    canvas = document.getElementById('map-canvas'),
    options = {
      zoom: sanLuis.defaultZoom,
      center: sanLuisLatLng,
      overviewMapControl: false,
      rotateControl: false,
      minZoom: sanLuis.minZoom,
      streetViewControl: false,
      scaleControl: false,
      zoomControl: false,
      panControl: false
    };

  map = new google.maps.Map(canvas, options);
  geocoder = new google.maps.Geocoder();
  initializeMarker(sanLuisLatLng, 'Origen', 'begin');
  initializeMarker(sanLuisLatLng, 'Destino', 'end');
  initializeStartAndFinishMarkers();
  _.each(paths, function (path) {
    renderPath(path.points, 0.2, path.color, 4, 5);
  });
  
  //initializeLine();
  initializeMarkersListener = google.maps.event.addListener(map, 'click', handleClickOnMap);
  initializeForm();
}

$(function () {
  loadPaths(function () {
    google.maps.event.addDomListener(window, 'load', initialize);
  });
});


