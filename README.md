# Setuping the application

## Set the scope variables
Put the next configuration in your .bachrc file

	export PATH=$PATH:PATH_TO_PROJECT/grails/bin
	export GRAILS_HOME=PATH_TO_PROJECT/grails
	export JAVA_HOME=PATH_TO_JAVA_FOLDER

Code example

	export PATH=$PATH:/home/javier/git/que-colectivo-server/grails/bin
	export GRAILS_HOME=/home/javier/git/que-colectivo-server/grails
	export JAVA_HOME=/usr/lib/jvm/java-6-openjdk-i386

## Start mysql service
Code example

	mysql.server start	

## Setup the database
Import the file: [/db/queColectivo.sql](https://bitbucket.org/sljavi/que-colectivo-server/src/0e4ae393b8980a7eaf24787665df10c5a156dd81/db/queColectivo.sql?at=master#cl-1)

- Server: localhost
- Database: queColectivo
- User: dev
- Password: devpw

## Start the application 
Code example

	cd /home/javier/git/que-colectivo-server/que-colectivo
	grails run-app

## Open the application
- Backend: [http://localhost:8080/que-colectivo/path/index](http://localhost:8080/que-colectivo/path/index)
- Frontend: [http://localhost:8080/que-colectivo/map/index](http://localhost:8080/que-colectivo/map/index)
